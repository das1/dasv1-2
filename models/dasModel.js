var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var trainerDeatils = new Schema({
    trainerId: String,
    password: String
})

var studentAttendance = new Schema({
    studentId: String,
    dateOfAttendance: Date,
    sessionId: String,
    trainerId: String,
    gender: String
})

var studentRegistration = new Schema({
    studentId: String,
    fpScan: String,
    dateOfRegistration: Date,
    trainerId: String,
    gender: String
})

var trainerDeatilsSchemaMongoose = mongoose.model('trainerdeatilsmodelschemas', trainerDeatils);
var studentAttendanceSchemaMongoose = mongoose.model('studentattendancemodelschemas', studentAttendance);
var studentRegistrationSchemaMongoose = mongoose.model('studentregistrationmodelschemas', studentRegistration);

module.exports = {
    trainerDeatilsModal: trainerDeatilsSchemaMongoose,
    studentAttendanceModal: studentAttendanceSchemaMongoose,
    studentRegistrationModal: studentRegistrationSchemaMongoose,
};

var db = mongoose.connection;
db.on('error', console.error)